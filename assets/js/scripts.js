
function fetch_users()
{
   $.ajax({
    url:"../Users/get_users",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#users').DataTable();
    }
   });  
}

function fetch_submitted_documents()
{
   $.ajax({
    url:"../Submitted_documents/get_submitted_documents",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#submitted_documents').DataTable();
    }
   });  
}

function fetch_sort(column,value,link='',tableid='')
{
   $.ajax({
    url:link,
    method:"POST",
    data:{column: column , value: value},
    dataType:"text",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#'+tableid).DataTable();
    }
   });  
}

  //Sort Functions for USERS
    $(document).on('click','#btn_sort_all_users',function()
    {
      notify('Sort by all users','success');
      fetch_users();
    });

  $(document).on('click','#btn_sort_active_users',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Users/get_sort_users";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'users');
        notify('Sort by active users','success');
      }
    });

    $(document).on('click','#btn_sort_inactive_users',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Users/get_sort_users";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'users');
        notify('Sort by inactive users','success');
      }
    });

$(document).on('click','.btn_sort_role',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var role = $(this).data("role");
      var link = "../Users/get_sort_users";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'users');
        notify('Sort by '+role,'success');
      }
    });
 //End Sort Functions for USERS

 //Sort Functions for SCHOOLS
 $(document).on('click','#btn_sort_all_schools',function()
    {
      notify('Sort by all schools','success');
      fetch_schools();
    });

  $(document).on('click','#btn_sort_active_schools',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Schools/get_sort_schools";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'schools');
        notify('Sort by active schools','success');
      }
    });

     $(document).on('click','#btn_sort_inactive_schools',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Schools/get_sort_schools";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'schools');
        notify('Sort by inactive schools','success');
      }
    });

    $(document).on('click','#btn_sort_assigned_schools',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Schools/get_sort_schools";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'schools');
        notify('Sort by assigned schools','success');
      }
    });

    $(document).on('click','#btn_sort_unassigned_schools',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Schools/get_sort_schools";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'schools');
        notify('Sort by unassigned schools','success');
      }
    });

//End Sort Functions for SCHOOLS

// Sort Functions for ROLES
 $(document).on('click','#btn_sort_all_roles',function()
    {
      notify('Sort by all roles','success');
      fetch_roles();
    });

    $(document).on('click','#btn_sort_active_roles',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Roles/get_sort_roles";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'roles');
        notify('Sort by active roles','success');
      }
    });

    $(document).on('click','#btn_sort_inactive_roles',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Roles/get_sort_roles";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'roles');
        notify('Sort by inactive roles','success');
      }
    });
//End Sort Functions for ROLES

// Sort Functions for ACCREDITATION
 $(document).on('click','#btn_sort_all_accreditations',function()
    {
      notify('Sort by all accreditation level','success');
      fetch_accreditations();
    });

    $(document).on('click','#btn_sort_active_accreditations',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Accreditations/get_sort_accreditations";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'accreditations');
        notify('Sort by active accreditation level','success');
      }
    });

    $(document).on('click','#btn_sort_inactive_accreditations',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Accreditations/get_sort_accreditations";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'accreditations');
        notify('Sort by inactive accreditation level','success');
      }
    });
//End Sort Functions for ACCREDITATION

// Sort Functions for LEVELS
 $(document).on('click','#btn_sort_all_levels',function()
    {
      notify('Sort by all levels','success');
      fetch_levels();
    });

    $(document).on('click','#btn_sort_active_levels',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Levels/get_sort_levels";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'levels');
        notify('Sort by active levels','success');
      }
    });

    $(document).on('click','#btn_sort_inactive_levels',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Levels/get_sort_levels";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'levels');
        notify('Sort by inactive levels','success');
      }
    });
//End Sort Functions for LEVELS

// Sort Functions for PROGRAMS
 $(document).on('click','#btn_sort_all_programs',function()
    {
      notify('Sort by all programs','success');
      fetch_programs();
    });

    $(document).on('click','#btn_sort_active_programs',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Programs/get_sort_programs";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'programs');
        notify('Sort by active programs','success');
      }
    });

    $(document).on('click','#btn_sort_inactive_programs',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Programs/get_sort_programs";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'programs');
        notify('Sort by inactive programs','success');
      }
    });
//End Sort Functions for PROGRAMS

// Sort Functions for AREAS
 $(document).on('click','#btn_sort_all_area',function()
    {
      notify('Sort by all areas','success');
      fetch_areas();
    });

    $(document).on('click','#btn_sort_active_area',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Areas/get_sort_areas";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'areas');
        notify('Sort by active areas','success');
      }
    });

    $(document).on('click','#btn_sort_inactive_area',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Areas/get_sort_areas";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'areas');
        notify('Sort by inactive areas','success');
      }
    });
//End Sort Functions for AREAS

// Sort Functions for SECTIONS
 $(document).on('click','#btn_sort_all_sections',function()
    {
      notify('Sort by all sections','success');
      fetch_sections();
    });

    $(document).on('click','#btn_sort_active_sections',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Sections/get_sort_sections";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'sections');
        notify('Sort by active sections','success');
      }
    });

    $(document).on('click','#btn_sort_inactive_sections',function()
    {
      var column = $(this).data("column");
      var value = $(this).data("value");
      var link = "../Sections/get_sort_sections";
      if(confirm("Are you sure you ?"))
      {
        fetch_sort(column,value,link,'sections');
        notify('Sort by inactive sections','success');
      }
    });
//End Sort Functions for SECTIONS

 function fetch_schools()
{
   $.ajax({
    url:"../Schools/get_schools",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#schools').DataTable();
    }
   });  
}

function fetch_programs()
{
   $.ajax({
    url:"../Programs/get_programs",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#programs').DataTable(
        {
          "order": []
        });
    }
   });  
}

function fetch_levels()
{
   $.ajax({
    url:"../Levels/get_levels",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#levels').DataTable();
    }
   });  
}


function fetch_areas()
{
   $.ajax({
    url:"../Areas/get_areas",
     method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
       $('#areas').DataTable();
    }
   });  
}

function fetch_communications()
{
   $.ajax({
    url:"../Communications/get_communications",
     method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
       $('#communications').DataTable();
    }
   });  
}

function fetch_send_letter()
{
   $.ajax({
    url:"../Send_letter/get_send_letter",
     method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
       $('#send_letter').DataTable();
    }
   });  
}


function fetch_roles()
{
   $.ajax({
    url:"../Roles/get_roles",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
       $('#roles').DataTable();
    }
   });  
}


function fetch_accreditations()
{
   $.ajax({
    url:"../Accreditations/get_accreditations",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#accreditations').DataTable();
      $('#roles').DataTable();

    }
   });  
}

function fetch_ssi_school()
{
   $.ajax({
    url:"../Req_ssi/get_ssi_school",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#ssi_school').DataTable();
    }
   });  
}

function fetch_list_req_ssi()
{
   $.ajax({
    url:"../Ssi/get_list_req_ssi",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#ssi_school').DataTable();
    }
   });  

   
}

function fetch_ssi_request()
{
   $.ajax({
    url:"../Req_ssi/get_ssi_school",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#ssi_school').DataTable();
    }
   });  
}

function fetch_ssi_manage()
{
   $.ajax({
    url:"../Manage_ssi/get_ssi_school",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#ssi_school').DataTable();
    }
   });  
}

function fetch_assign_accreditor()
{
   $.ajax({
    url:"../Assign_accreditor/get_assign_accreditor",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#submitted_ssi').DataTable();
    }
   });  
}

function fetch_ssi_view()
{
   $.ajax({
    url:"../Ssi_view/get_ssi_school",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#ssi_school').DataTable();
    }
   });  
}

function fetch_check_req_ssi()
{
   $.ajax({
    url:"../Ssi/get_check_req_ssi",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#check_req_ssi').DataTable();
    }
   });  
}

function fetch_final_ssi()
{
   $.ajax({
    url:"../Ssi/get_final_ssi",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#final_ssi').DataTable();
    }
   });  
}

function fetch_board_of_directors()
{
   $.ajax({
    url:"../Board_of_directors/get_board_of_directors",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#bod').DataTable();
    }
   });  
}

function fetch_submitted_ssi()
{
   $.ajax({
    url:"../Ssi/get_submitted_ssi",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#submitted_ssi').DataTable();
    }
   });  
}

function fetch_price_ssi()
{
   $.ajax({
    url:"../Price_ssi/get_price_ssi",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#price_ssi').DataTable();
    }
   });  
}

function fetch_billings()
{
   $.ajax({
    url:"../Billings/get_billings",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#billings').DataTable();
    }
   });  
}

function fetch_notes()
{
   $.ajax({
    url:"../Notes/get_notes",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#notes').DataTable();
    }
   });  
}

function fetch_audit()
{
   $.ajax({
    url:"../Audit/get_audit",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#audit').DataTable();
    }
   });  
}

function notify(mes,mes_type)
{
    $.notify({
    // options
      message: mes 
    },{
      // settings
      type: mes_type,
      placement: {
        from: "bottom",
        align: "right"
      },
      delay:2000,
      animate: {
        enter: 'animated fadeInDown',
        exit: 'animated fadeOutUp'
      },
  });
}

$(document).ready(function(){
  

  $("#forgot_pass").click(function(){
      $("#forgot_pass_modal").modal({
        "backdrop": "static" 
      });
  });


  $('#forgotten_account_form').on('submit',function(e){
      e.preventDefault();
      var post_url = 'Home/send_forgot_password';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#forgotten_account_form').serialize(),
          beforeSend:function(){
            $('#feedback2').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#forgot_pass_btn').hide();
          },
          success : function(data){
              $('#feedback2').html('');
              $('#forgotten_account_form')[0].reset();
              $('#forgot_pass_btn').show(500);
              $('#forgot_pass_modal').modal('toggle');
              if(data == 'success')
              {
                notify('Please check your email and click the link.','success'); 
              }
              else
              {
                notify('Email does not exists.','danger'); 
              }
                            
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });

  $('#forgotten_account_form2').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Users/send_forgot_password';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#forgotten_account_form2').serialize(),
          beforeSend:function(){
            $('#feedback2').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#forgot_pass_btn').hide();
          },
          success : function(data){
              $('#feedback2').html('');
              $('#forgotten_account_form2')[0].reset();
              $('#forgot_pass_btn').show(500);
              $('#forgot_pass_modal').modal('toggle');
                fetch_users();
              if(data == 'success')
              {
                
                notify('Please check your email and click the link.','success'); 
              }
              else
              {
                notify('Email does not exists.','danger'); 
              }
                            
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });

  $('#add_user_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Users/add_user';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#add_user_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#add_user_btn').hide();
          },
          success : function(data){
              $('#feedback').html('');
              if(data=='success')
              {
                $('#add_user_form')[0].reset();
                $('#add_user_btn').show(500);
                $('#add_user_modal').modal('toggle');
                 $('#error').html('');
                fetch_users();
                notify('Successfully added','success'); 
              }
              else
              {
                $('#error').html(data);
                 $('#add_user_btn').show(500);
              }
                            
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });



      

   $(document).on('click','.copy_ssi_btn',function()
    {
        $("#copy_ssi_modal").modal({
          "backdrop": "static" 
        });
         var title = $(this).data('title');
         var ssi_code = $(this).data('ssi');
         var program = $(this).data('program');
         $("#title").val(title);
         $("#ssi_code").val(ssi_code);
         $("#program").val(program);
    });

    $(document).on('click','.request_disapp',function()
    {
        $("#modal_disapproved").modal({
          "backdrop": "static" 
        });
         var req = $(this).data('req');
         var school = $(this).data('school');
         $("#req").val(req);
         $("#school").val(school);
         
    });


   $(document).on('click','.update_ssi_btn',function()
    {
        $("#update_ssi_modal").modal({
          "backdrop": "static" 
        });
         var title = $(this).data('title');
         var ssi_code = $(this).data('ssi');
         var program = $(this).data('program');
         var curprogram = $(this).data('curprogram');
         var board = $(this).data('board');
         $("#title2").val(title);
         $("#ssi_code2").val(ssi_code);
         $("#program2").val(program);
         $("#curprogram").val(curprogram);
         if(board==1)
         {
          $("#board_exam_2").prop('checked', true);
         }
    });
    

   $(document).on('click','.ssi_submitted_approve_id',function()
    {
        
         var id = $(this).data('id');
       
         $("#id").val(id);
         $("#id2").val(id);
         $("#id3").val(id);
    });

    $(document).on('click','.send_comm',function()
    {
        
         var id = $(this).data('id');
         var email = $(this).data('email');
      
         $("#id2").val(id);
         $("#email").val(email);
    });


    $(document).on('click','.send_lett',function()
    {
        
         var id = $(this).data('id');
         var email = $(this).data('email');
      
         $("#id2").val(id);
         $("#email").val(email);
    });



$('#update_ssi_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Ssi/update_ssi';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#update_ssi_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#update_ssi').hide();
          },
          success : function(data){
              $('#feedback2').html('');
              $('#update_ssi').show(500);
              if(data=='success')
              {
                notify('Successfully updated','success');
                fetch_ssi();
                $('#update_ssi_modal').modal('toggle');
              }
              else
              {
                notify(data,'danger');
              }

          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });
  $('#check_ssi_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../req_check_ssi';
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
            type : 'POST',
            url : post_url,
            data: $('#check_ssi_form').serialize(),
            beforeSend:function(){
              $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
              $('#check_ssi_btn').hide();
            },
            success : function(data){
                $('#feedback').html('');
                $('#check_ssi_btn').show(500);
                if(data=='success')
                {
                  notify('Successfully requested for checking','success');
                }
                else
                {
                  notify(data,'danger');
                }
                
            },
            error : function() {
                 notify('Error in submit ','danger');
            }
        });
      }
  });


   $('#copy_ssi_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Ssi/copy_ssi';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#copy_ssi_form').serialize(),
          beforeSend:function(){
            $('#feedback2').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#copy_ssi').hide();
          },
          success : function(data){
              $('#feedback2').html('');
              $('#copy_ssi').show(500);
              if(data=='success')
              {
                $('#copy_ssi_form')[0].reset();
                $('#copy_ssi_modal').modal('toggle');
                $('#error2').html('');
                fetch_ssi();
                notify('SSI Successfully copied. Please wait for approval.','success');
              }else
              {
                $('#error2').html(data);
                notify(data,'danger');
              } 
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });




  $("#add_user").click(function(){
      $("#add_user_modal").modal({
        "backdrop": "static" 
      });
  });

  $("#update_user_btn").click(function(){
      $("#update_user_modal").modal({
        "backdrop": "static" 
      });
  });

    $(document).on('click','.update_user_btn',function()
    {
         var fname = $(this).data('fname');
         var mname = $(this).data('mname');
         var lname = $(this).data('lname');
         var email = $(this).data('email');
         var address = $(this).data('address');
         var contact_num = $(this).data('contact');
         var role = $(this).data('role');
         var user_id = $(this).data('id');
         var privileges = $(this).data('privileges');
         $('#privileges').selectpicker('val', privileges.split(","));
        $("#fname").val(fname);
        $("#mname").val(mname);
        $("#lname").val(lname);
        $("#email").val(email);
        $("#user_id").val(user_id);
        $("#address").val(address);
        $("#role").val(role);
        $(".modal-body #contact_num").val( contact_num );
    });

    $('#update_user_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Users/update_user';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#update_user_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#update_user_btn').hide();
          },
          success : function(data){
              $('#feedback').html('');
              $('#update_user_btn').show(500);
              if(data=='success'){
              $('#update_user_form')[0].reset();
              $('#update_user_modal').modal('toggle');
              $('#error2').html('');
              fetch_users();
              notify('Successfully updated','success');
              }else
              {
                $('#error2').html(data);
                notify('Please check your input.','danger');
              } 
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });



  $(document).on('click','.btn_update',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Users/update_status",
          method:"POST",
          data:{id: id , status: status},
          dataType:"text",
          success:function(data)
          {
            fetch_users();
            notify('Successfully updated','success');
          }
        });
      }
    });

  //Roles
  $(document).on('click','.btn_update_role',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Roles/update_status",
          method:"POST",
          data:{id: id , status: status},
          dataType:"text",
          success:function(data)
          {
            fetch_roles();
            notify('Successfully updated','success');
          }
        });
      }
    });

  $("#add_user_role").click(function(){
      $("#add_user_role_modal").modal({
        "backdrop": "static" 
      });
  });


  $(document).on('click','.update_user_role_btn',function()
  {
       var role = $(this).data('role');
       var privileges = $(this).data('privileges').toString();
           var role_id = $(this).data('id');
       if( privileges.indexOf(',') >= 0){
          $('#privileges').selectpicker('val', privileges.split(","));
          
        }
        else
        {
          $('#privileges').selectpicker('val',privileges );
        }
       $("#role2").val(role);
       $("#role_id").val(role_id);
  });

  $('#update_user_role_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Roles/update_role';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#update_user_role_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#update_user_role_btn').hide();
          },
          success : function(data){
            if(data == 'success'){
              $('#feedback').html('');
              $('#update_user_role_form')[0].reset();
              $('#update_user_role_btn').show(500);
              $('#update_user_role_modal').modal('toggle');
              fetch_roles();
              notify('Successfully updated','success'); 
            }else{
              $('#error2').html(data);
                notify('Please check your input.','danger');
                $('#feedback').html('');
                $('#update_user_role_btn').show(500);
            }
         },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });
    

  //School

  //Insert School
   $("#add_school").click(function(){
      $("#modal_school_add").modal({
        "backdrop": "static" 
      });
  });

  $('#add_user_role_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Roles/add_role';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#add_user_role_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#add_user_role_btn').hide();
          },
          success : function(data){
            if(data == 'success'){
              $('#feedback').html('');
              $('#add_user_role_form')[0].reset();
              $('#add_user_role_btn').show(500);
              $('#error').html('');
              $('#add_user_role_modal').modal('toggle');
              fetch_roles();
              notify('Successfully added','success');
            }else{
               $('#error').html(data);
                $('#feedback').html('');
                $('#add_user_role_btn').show(500);
            }

          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });



   $('#add_school_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Schools/add_school';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#add_school_form').serialize(),
          beforeSend:function(){
          },
          success : function(data){
               $("#add_school_form")[0].reset();
               $('#modal_school_add').modal('toggle');
               notify('Successfully added','success');
               fetch_schools();
          },
          error : function() {
             notify('Error in submit ','danger');
          }
      });
  });

  $(document).on('click','.btn_school_update',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Schools/update_status",
          method:"POST",
          data:{id: id , status: status},
          dataType:"text",
          success:function(data)
          {
            fetch_schools();
            notify('Successfully updated','success');
          }
        });
      }
    });

     $(document).on('click','.update_school_btn',function()
    {
         var school_name = $(this).data('schoolname');
         var school_address = $(this).data('schooladdress');
         var school_date = $(this).data('schooldate');
         var school_website = $(this).data('schoolwebsite');
         var school_ched = $(this).data('schoolched');
         var schooltelnum = $(this).data('schooltelnum');
         var school_assign = $(this).data('schoolassign');
         var school_id = $(this).data('schoolid');
         var region = $(this).data('region');   
         var college =  $(this).data('college');  
         var presname = $(this).data('presname');
         var presemail = $(this).data('presemail');
         var presdesignation = $(this).data('presdesignation');
   
        $("#edit_assign").val(school_assign);
        $("#edit_name").val(school_name);
         $("#edit_college").val(college);
        $("#edit_address").val(school_address);
        $("#edit_date").val(school_date);
        $("#edit_website").val(school_website);
        $("#edit_ched").val(school_ched);
        $("#edit_tel_num").val(schooltelnum);
        $("#edit_id").val(school_id);
        $("#edit_region").val(region);
        $("#edit_pres_name").val(presname);
        $("#edit_pres_email").val(presemail);
        $("#edit_pres_designation").val(presdesignation);

    });

     $(document).on('click','.notes_view',function()
    {
         var id = $(this).data('id');
         var notesfrom = $(this).data('notesfrom');
         var notesdesc = $(this).data('notesdesc');
         var created = $(this).data('created');
         
        $("#view_id").val(id);
         $("#view_notesfrom").val(notesfrom);
        $("#view_notesdesc").val(notesdesc);
        $("#view_created").val(created);   
        
    });

    $('#edit_school_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Schools/update_school';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#edit_school_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#update_school_btn').hide();
          },
          success : function(data){
              $('#feedback').html('');
              $('#edit_school_form')[0].reset();
              $('#update_school_btn').show(500);
              $('#modal_school_edit').modal('toggle');
              fetch_schools();
              notify('Successfully updated','success'); 
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });

 

   $('#login_form').on('submit',function(e)
   {
      e.preventDefault();
      var post_url = 'Home/login';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#login_form').serialize(),
          beforeSend:function(){
             $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
              $('#sign_in_btn').hide();
          },
          success : function(data){
              if(data=='success')
              {
                  $(location).attr('href', 'Home/dashboard');
              }
              else
              {
                $('#feedback').html('');
                notify('Incorrect username or password','danger');
                $('#login_form')[0].reset();
                $('#sign_in_btn').show(500);
              }
          },
          error : function() {
              $('#modal_content').html('<p class="error">Error in submit</p>');
          }
      });
   });

   function fetch_data(url,id)
  {
    $.ajax({
      url:url,
      method:"POST",
      success:function(data)
      {
        $("#live_data").html(data);
        $(id).DataTable();
      }
     });  
  }

  //Insert Program
   $("#add_program").click(function(){
      $("#add_program_modal").modal({
        "backdrop": "static" 
      });
  });


   $('#add_program_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Programs/add_program';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#add_program_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#add_program_btn').hide();
          },
          success : function(data){
            if(data == 'success'){
               $("#add_program_form")[0].reset();
               $('#error').html('');
               $('#add_program_modal').modal('toggle');
               $('#add_program_btn').show(500);
               notify('Successfully added!','success');
               fetch_programs();
            }else{
              $('#error').html(data);
                 $('#add_program_btn').show(500);
            }
          },
          error : function() {
              notify('Error in submit ','danger');
          }
      });
  });

   $(document).on('click','.btn_program_update',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Programs/update_status",
          method:"POST",
          data:{id: id , status: status},
          dataType:"text",
          success:function(data)
          {
            fetch_programs();
            notify('Successfully updated','success');
          }
        });
      }
    });

    $(document).on('click','.update_program_btn',function()
    {
         var name = $(this).data('name');
         var id = $(this).data('programid');
         var level = $(this).data('level');
         var acc = $(this).data('acc');
        $("#edit_name").val(name);
        $("#edit_id").val(id);
       $("#edit_level").val(level);
       $('#edit_acc').selectpicker('val', acc.split(","));
    });

    $('#edit_program_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Programs/update_program';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#edit_program_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#update_program_btn').hide();
          },
          success : function(data){
            if(data == 'success'){
              $('#feedback').html('');
              $('#edit_program_form')[0].reset();
              $('#update_program_btn').show(500);
              $('#modal_program_edit').modal('toggle');
              fetch_programs();
              notify('Successfully updated','success'); 
            }else{
               $('#error2').html(data);
                notify('Please check your input.','danger');
                $('#feedback').html('');
                $('#update_program_btn').show(500);
            }
        },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });

   $(document).on('click','.view_program_btn',function()
    {
         var name = $(this).data('name');
         var acc = $(this).data('acc');
         var level = $(this).data('level');
         $("#view_name").val(name);
         $("#view_acc").val(acc);
         $("#view_level").val(level);
    });

    //Levels
    //Insert Level
   $("#add_level").click(function(){
      $("#add_level_modal").modal({
        "backdrop": "static" 
      });
  });


   $('#add_level_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Levels/add_level';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#add_level_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#add_level_btn').hide();
          },
          success : function(data){
            if(data == 'success'){
               $("#add_level_form")[0].reset();
                $('#error').html('');
               $('#add_level_modal').modal('toggle');
               notify('Successfully added!','success');
               fetch_levels();
            }else{
               $('#error').html(data);
              $('#feedback').html('');
              $('#add_level_btn').show();
            }
          },
          error : function() {
              notify('Error in submit ','danger');
          }
      });
  });

   $(document).on('click','.btn_level_update',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Levels/update_status",
          method:"POST",
          data:{id: id , status: status},
          dataType:"text",
          success:function(data)
          {
            fetch_levels();
            notify('Successfully updated','success');
          }
        });
      }
    });

    $(document).on('click','.update_level_btn',function()
    {
         var name = $(this).data('name');
         
         var id = $(this).data('levelid');
        $("#edit_name").val(name);
        
       
        $("#edit_id").val(id);
        console.log(id);
        
    });

    $('#edit_level_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Levels/update_level';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#edit_level_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#update_level_btn').hide();
          },
          success : function(data){
            if(data == 'success'){
              $('#feedback').html('');
              $('#edit_level_form')[0].reset();
              $('#update_level_btn').show(500);
              $('#modal_level_edit').modal('toggle');
              fetch_levels();
              notify('Successfully updated','success'); 
            }else{
               $('#error2').html(data);
                notify('Please check your input.','danger');
                $('#update_level_btn').show(500);
                $('#feedback').html('');
            }
         },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });

    //Area 
    //Insert Area
   $("#add_area").click(function(){
      $("#add_area_modal").modal({
        "backdrop": "static" 
      });
       $("#name").val('');
       $('#area_content').summernote('code', '');
  });


   $('#add_area_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Areas/add_area';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#add_area_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#add_area_btn').hide();
          },
          success : function(data){
               $("#add_area_form")[0].reset();
               $('#add_area_btn').show();
               $('#add_area_modal').modal('toggle');
               notify('Successfully added!','success');
               fetch_areas();
          },
          error : function() {
              notify('Error in submit ','danger');
          }
      });
  });

  $(document).on('click','.btn_area_update',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Areas/update_status",
          method:"POST",
          data:{id: id , status: status},
          dataType:"text",
          success:function(data)
          {
            fetch_areas();
            notify('Successfully updated','success');
          }
        });
      }
    });

    $(document).on('click','.update_area_btn',function()
    {
         var name = $(this).data('name');
         var id = $(this).data('id');
         var level = $(this).data('level');
         var program = $(this).data('program');
         var content = $(this).data('content');
          var weight = $(this).data('weight');
         console.log(program)
        $("#edit_name").val(name);
        $("#edit_id").val(id);
        $("#edit_level").val(level);
        $("#edit_program").val(program);
         $("#edit_weight").val(weight);
         $('#edit_area').summernote('code', content);
        
    });

    $(document).on('click','.copy_area_btn',function()
    {
         var name = $(this).data('name');
         var id = $(this).data('id');
         var program = $(this).data('program');
         var level = $(this).data('level');
         var content = $(this).data('content');
         var weight = $(this).data('weight');
         console.log(program);
        $("#name").val(name);
        $("#weight").val(weight);
        $("#program").val(program);
        // $("#acc_level").val(level);
        $('#area_content').summernote('code', content);
        
        // $("#area_content").val(content);
        
    });


    $(document).on('click','.view_area_btn',function()
    {
         var name = $(this).data('name');
         var program = $(this).data('program');
         var level = $(this).data('level');
         var weight = $(this).data('weight');
         var content = $(this).data('content');
         $("#view_name").val(name);
         $("#view_program").val(program);
         $("#view_level").val(level);
         $("#view_weight").val(weight);
         $("#view_content").summernote('code', content);
         console.log(content);
    });

    $('#edit_area_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Areas/update_area';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#edit_area_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#update_area_btn').hide();
          },
          success : function(data){
              $('#feedback').html('');
              $('#edit_area_form')[0].reset();
              $('#update_area_btn').show(500);
              $('#modal_area_edit').modal('toggle');
              fetch_areas();
              notify('Successfully updated','success'); 
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });


    //Accreditation 
    //Insert Accreditation
   $("#add_accreditation").click(function(){
      $("#add_accreditation_modal").modal({
        "backdrop": "static" 
      });
  });


   $('#add_accreditation_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Accreditations/add_accreditation';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#add_accreditation_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#add_accreditation_btn').hide();
          },
          success : function(data){
            if(data == 'success'){
               $("#add_accreditation_form")[0].reset();
               $('#add_accreditation_btn').show();
                $('#error').html('');
               $('#add_accreditation_modal').modal('toggle');
               notify('Successfully added!','success');
               fetch_accreditations();
            }
            else{
              $('#error').html(data);
              $('#feedback').html('');
              $('#add_accreditation_btn').show();
            }
          },
          error : function() {
              notify('Error in submit ','danger');
          }
      });
  });

  $(document).on('click','.btn_accreditation_update',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Accreditations/update_status",
          method:"POST",
          data:{id: id , status: status},
          dataType:"text",
          success:function(data)
          {
            fetch_accreditations();
            notify('Successfully updated','success');
          }
        });
      }
    });

    $(document).on('click','.update_accreditation_btn',function()
    {
         var title = $(this).data('title');
         var desc = $(this).data('desc');
         var accredid = $(this).data('accredid');
        $("#edit_title").val(title);
        $("#edit_desc").val(desc);
        $("#edit_accredid").val(accredid);    
    });

    $('#edit_accreditation_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Accreditations/update_accreditation';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#edit_accreditation_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#update_accreditation_btn').hide();
          },
          success : function(data){
            if(data == 'success'){
              $('#feedback').html('');
              $('#edit_accreditation_form')[0].reset();
              $('#update_accreditation_btn').show(500);
              $('#modal_accreditation_edit').modal('toggle');
              fetch_accreditations();
              notify('Successfully updated','success'); 
            }else{
               $('#error2').html(data);
                notify('Please check your input.','danger');
                $('#feedback').html('');
                 $('#update_accreditation_btn').show(500);
            }
         },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });

  //School SSI
  $('#add_req_ssi_form').on('submit',function(e){
    console.log('s');
      e.preventDefault();
      var post_url = '../Req_ssi/add_request_ssi';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: new FormData($('#add_req_ssi_form')[0]),
          cache: false,
          contentType: false,
          processData: false,

          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#add_req_ssi_btn').hide();
          },
          success : function(data){
            console.log(data);
              $('#feedback').html('');
              $('#add_req_ssi_form')[0].reset();
              $('#add_req_ssi_btn').show(500);
              $('#requests_modal').modal('toggle');
              fetch_ssi_school();
              notify('Request has been send','success');               
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
        });

        $('#add_orient_form').on('submit',function(e){
        
            e.preventDefault();
            var post_url = '../Req_ssi/add_orientation_ssi';
            $.ajax({
                type : 'POST',
                url : post_url,
                data: new FormData($('#add_orient_form')[0]),
                cache: false,
                contentType: false,
                processData: false,
      
                beforeSend:function(){
                  $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
                  $('#add_orient_btn').hide();
                },
                success : function(data){
                  console.log(data);
                    $('#feedback').html('');
                    $('#add_orient_form')[0].reset();
                    $('#add_orient_btn').show(500);
                    $('#modal_orientation').modal('toggle');
                    fetch_ssi_school();
                    notify('Request has been send','success');               
                },
                error : function() {
                     notify('Error in submit ','danger');
                }
            });
              });



         $(document).on('click','#btn_list_req',function()
    {
         var id = $(this).data("id");
      var status = $(this).data("status");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Ssi/update_req_status",
          method:"POST",
          data:{id: id , status: status},
          dataType:"text",
          success:function(data)
          {
            fetch_list_req_ssi();
            notify('Successfully updated','success');
          }
        });
      }
    });

    $(document).on('click','#send_access_key',function()
    {
         var key = $(this).data("key");
      var email = $(this).data("email");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Ssi/send_mail",
          method:"POST",
          data:{key: key , email: email},
          dataType:"text",
          success:function(data)
          {
            fetch_list_req_ssi();
            notify('Successfully updated','success');
          }
        });
      }
    });

    $(document).on('click','#btn_resend_access',function()
    {
         var key = $(this).data("key");
      var email = $(this).data("email");
      if(confirm("Are you sure you want to resend access key?"))
      {
        $.ajax({
          url:"../Finance/resend_access_key",
          method:"POST",
          data:{key: key , email: email},
          dataType:"text",
          success:function(data)
          {
            fetch_list_req_ssi();
            notify('Successfully resend access key','success');
          }
        });
      }
    });

    $(document).on('click','#view_request',function()
    {
         var req = $(this).data("req");
      
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Ssi/get_request",
          method:"POST",
          data:{req: req},
          dataType:"text",
          success:function(data)
          {
            fetch_list_req_ssi();
            notify('Successfully updated','success');
          }
        });
      }
    });

    $(document).on('click','#btn_access_key',function()
    {
         var id = $(this).data('id');
         var level = $(this).data('level');
         var program = $(this).data('program');
         console.log(id);
         $("#id").val(id);
         $("#level").val(level);
         $("#program").val(program);
    });
    $(document).on('click','#btn_access_key_2',function()
    {
         var id = $(this).data('id');
         var level = $(this).data('level');
         var program = $(this).data('program');
         console.log(id);
         $("#id").val(id);
         $("#level").val(level);
         $("#program").val(program);
    });

     $(document).on('click','#btn_edit_price',function()
    {
         var id = $(this).data('id');
         var code = $(this).data('code');
         var title = $(this).data('title');
         var program = $(this).data('program');
         var price = $(this).data('price');
         
         $("#id").val(id);
         $("#code").val(code);
         $("#title").val(title);
          $("#program").val(program);
         $("#price").val(price);

    });

    $('#form_edit_price').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Price_ssi/edit_price';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#form_edit_price').serialize(),
          beforeSend:function(){
           
            
          },
          success : function(data){
              $('#feedback').html('');
              $('#form_edit_price')[0].reset();
              $('#modal_edit_price').modal('toggle');
              fetch_price_ssi();
              notify('Successfully edit','success');               
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });
    

     $(document).on('click','#btn_acc_key',function()
   {
      var num = $(this).data("num");
      var email = $(this).data("school");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../send_access_key",
          method:"POST",
          data:{num: num,email: email},
          dataType:"text",
          success:function(data)
          {
            $('#btn_acc_key').hide();
            $('#btn_succ_paid').show();
            console.log(data);
            // fetch_requested_ssi();
            notify('Successfully Release','success');
          }
        });
      }});
     
      $(document).on('click','.btn-submit-billing',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      var school = $(this).data("school");
     
      if(confirm("Are you sure you want to send the bill?"))
      {
        $.ajax({
          url:'../send_bill',
          method:"POST",
          data:{id: id, status: status,school: school},
          dataType:"text",
          success:function(data)
          {
            console.log(data);
            // fetch_requested_ssi();
             $('.btn-paid').show();
            $('.btn-submit-billing').hide();
            notify('Successfully send bill','success');
          }
        });
      }
    });

     $(document).on('click','.btn-approve-payment',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      var school = $(this).data("school");
     
      if(confirm("Are you sure you want to approve the bill?"))
      {
        $.ajax({
          url:'../approve_payment',
          method:"POST",
          data:{id: id, status: status,school: school},
          dataType:"text",
          success:function(data)
          {
            console.log(data);
            // fetch_requested_ssi();
            $('.btn-approve-payment').hide();
            notify('Successfully Approve Payment','success');
          }
        });
      }
    });

    $('#form_access_key').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Manage_ssi/access';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#form_access_key').serialize(),
          beforeSend:function(){
            // $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            // $('#access_key_btn').hide();
          },
          success : function(data){
              // $('#feedback').html('');
              $('#form_access_key')[0].reset();
              // $('#update_program_btn').show(500);
              // $('#form_access_key').modal('toggle');
              // fetch_ssi_manage();
 
              if(data == 'error'){
                notify('Wrong Access Key ','danger');
              }else{
               
                window.location.href = data;
              }
             
          },
          error : function(x) {
              console.log(x);
               notify('Error in submit ','danger');
          }
      });
  });

    $('#form_access_key2').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Ssi_view/access';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#form_access_key2').serialize(),
          beforeSend:function(){
            // $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            // $('#access_key_btn').hide();
          },
          success : function(data){
              // $('#feedback').html('');
              $('#form_access_key2')[0].reset();
              // $('#update_program_btn').show(500);
              // $('#form_access_key').modal('toggle');
              // fetch_ssi_manage();
 
              if(data == 'error'){
                notify('Wrong Access Key ','danger');
              }else{
                window.location.href = data;
              }
             
          },
          error : function(x) {
              console.log(x);
               notify('Error in submit ','danger');
          }
      });
  });
  $('#send_quot_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Finance/send_quot';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#send_quot_form').serialize(),
          beforeSend:function(){
           
            
          },
          success : function(data){
              $('#feedback').html('');
              $('#send_quot_form')[0].reset();
            
              // fetch_users();
              notify('Successfully send','success');               
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });


//section management
    $(document).on('click','.view_section_btn',function()
    {
         var title = $(this).data('title');
         var program = $(this).data('program');
         var level = $(this).data('level');
          var area = $(this).data('area');
         var weight = $(this).data('weight');
          var content = $(this).data('content');
         $("#view_title").val(title);
         $("#view_program").val(program);
         $("#view_level").val(level);
          $("#view_area").val(area);
         $("#view_weight").val(weight);
          $("#view_content").summernote('code', content);
    });

    $(document).on('click','.btn_section_update',function()
    {
      var id = $(this).data("id");
      var status = $(this).data("status");
      if(confirm("Are you sure you ?"))
      {
        $.ajax({
          url:"../Sections/update_status",
          method:"POST",
          data:{id: id , status: status},
          dataType:"text",
          success:function(data)
          {
            fetch_sections();
            notify('Successfully updated','success');
          }
        });
      }
    });

    $(document).on('click','.delete_section_btn',function()
    {
      var id = $(this).data("id");
      var title = $(this).data("title");
      if(confirm("Are you sure you want to delete?"))
      {
        $.ajax({
          url:"../Sections/delete",
          method:"POST",
          data:{id: id,title:title},
          dataType:"text",
          success:function(data)
          {
            
            if(data=='success')
            {
              notify('Successfully deleted','success');
            }
            else
            {
              notify(data,'danger');
            }
            fetch_sections();
          }
        });
      }
    });

     $(document).on('click','.update_section_btn',function()
    {
         var title = $(this).data('title');
         var program = $(this).data('program');
         var level = $(this).data('level');
          var area = $(this).data('area');
          var content = $(this).data('content');
         var weight = $(this).data('weight');
         var id = $(this).data('id');
         console.log(area);
        $("#edit_title").val(title);
        $("#edit_weight").val(weight);
        $("#edit_id").val(id);
        $("#edit_level").val(level);
        $("#edit_program").val(program).change();
        $("#edit_area").val(area).change();
         $('#edit_content').summernote('code', content);
        
    });

     $('#edit_section_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Sections/update_section';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#edit_section_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#edit_section_btn').hide();
          },
          success : function(data){
              $('#feedback').html('');
              $('#edit_section_form')[0].reset();
              $('#edit_section_btn').show(500);
              $('#modal_section_edit').modal('toggle');
              fetch_sections();
              notify('Successfully updated','success'); 
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });

  $(document).on('click','.copy_section_btn',function()
    {
         var title = $(this).data('title');
         var program = $(this).data('program');
         var level = $(this).data('level');
          var area = $(this).data('area');
         var weight = $(this).data('weight');
          var content = $(this).data('content');
         console.log(area);
        $("#section_name").val(title);
        $("#section_weight").val(weight);
        // $("#program").val(program);
        // $("#area").val(area);
        $('#section_content').summernote('code', content);
        
        // $("#area_content").val(content);
        
    });

//section management

$('#add_note_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Ssi/add_note';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#add_note_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#add_note_btn').hide();
          },
          success : function(data){
            console.log(data);
               $("#add_note_form")[0].reset();
               $('#add_note_btn').show();
               $('#modal_disapproved').modal('toggle');
               notify('Successfully send!','success');
              fetch_check_req_ssi();
          },
          error : function() {
              notify('Error in submit ','danger');
          }
      });
  });
    
    
});//End of document.ready



 //View School
  function view_school_modal(data){
      $("#modal_school_view").modal({
        "backdrop": "static" 
      });
      var get_url = '../Schools/get_school_row/'+data;
     
      $.ajax({
        url: get_url,
        dataType: 'JSON',
        type: 'GET',
        success : function(e){
          
         $("#view_name").val(e.data.school_name);
         $("#view_college").val(e.data.college);
         $("#view_address").val(e.data.school_address);
         $("#view_date").val(e.data.school_date);
         $("#view_website").val(e.data.school_website);
         $("#view_tel_num").val(e.data.school_tel_num);
         $("#view_ched").val(e.data.school_ched);
         $("#view_assign").val(e.data.school_assign);
         $("#view_region").val(e.data.school_region);
         $("#view_pres_name").val(e.data.president_name);
         $("#view_pres_email").val(e.data.president_email);
         $("#view_pres_designation").val(e.data.president_designation);

      }
      });     
  }//end if view school modal

  

  //Edit School
  function edit_school_modal(data){
      $("#modal_school_edit").modal({
        "backdrop": "static" 
      });
      var get_url = '../Schools/get_school_row/'+data;
     
      $.ajax({
        url: get_url,
        dataType: 'JSON',
        type: 'GET',
        success : function(e){
          
         $("#edit_name").val(e.data.school_name);
         $("#edit_address").val(e.data.school_address);
          $("#edit_college").val(e.data.college);
         $("#edit_date").val(e.data.school_date);
         $("#edit_website").val(e.data.school_website);
         $("#edit_tel_num").val(e.data.school_tel_num);
         $("#edit_ched").val(e.data.school_ched);
         $("#edit_assign").val(e.data.school_assign);
         $("#edit_region").val(e.data.school_region);
         $("#edit_pres_name").val(e.data.president_name);
         $("#edit_pres_email").val(e.data.president_email);
        
      }
      });     
  }//end if view school modal

 

   //View Level
  function view_level_modal(data){
      $("#modal_level_view").modal({
        "backdrop": "static" 
      });
      var get_url = '../Levels/get_level_row/'+data;
     
      $.ajax({
        url: get_url,
        dataType: 'JSON',
        type: 'GET',
        success : function(e){
          
         $("#view_name").val(e.data.level_name);
         $("#view_area").val(e.data.level_area);
        
      }
      });     
  }//end if view level modal


   //View Level
  function view_area_modal(data){
      $("#modal_area_view").modal({
        "backdrop": "static" 
      });
      var get_url = '../Areas/get_area_row/'+data;
     
      $.ajax({
        url: get_url,
        dataType: 'JSON',
        type: 'GET',
        success : function(e){
          
         $("#view_name").val(e.data.area_name);       
      }
      });     
  }//end if view area modal

   //View Accreditation
  function view_accreditation_modal(data){
      $("#modal_accreditation_view").modal({
        "backdrop": "static" 
      });
      var get_url = '../Accreditations/get_accreditation_row/'+data;
     
      $.ajax({
        url: get_url,
        dataType: 'JSON',
        type: 'GET',
        success : function(e){
          
         $("#view_title").val(e.data.accred_title);
         $("#view_desc").val(e.data.accred_desc);
        
      }
      });     
  }//end if view level modal


  //ssi
  
  $("#create_ssi").click(function(){
      $("#create_ssi_modal").modal({
        "backdrop": "static" 
      });
  });

  $("#list_ssi").click(function(){
      fetch_ssi();
  });

  $("#list_req_ssi").click(function(){
      fetch_list_req_ssi();
  });

  function fetch_ssi()
  {
     $.ajax({
      url:"../Ssi/get_ssi",
      method:"POST",
      success:function(data)
      {
        $("#live_data").html(data);
        $('#ssi').DataTable();
      }
     });  
  }

  $('#add_section_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Sections/add_section';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#add_section_form').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#add_section_btn').hide();
          },
          success : function(data){
              $('#feedback').html('');
              $('#add_section_form')[0].reset();
              $('#section_content').summernote('code', '');
              $('#add_section_btn').show(500);
              fetch_sections();
              $('.selectpicker').selectpicker('refresh');
              $('#add_section_modal').modal('toggle');
              notify('Successfully added','success');               
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });


//section management
  $("#add_section").click(function(){
      $("#add_section_modal").modal({
        "backdrop": "static" 
      });
  });


  function fetch_sections()
{
   $.ajax({
    url:"../Sections/get_sections",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#sections').DataTable();
    }
   });  
}

function check_duplication(val){
        $.ajax({
            type: "POST",
            url: 'check_duplication',
            data:'id='+ val,
            success:function(data){
                $("#modal_content2").html(data);
            }


        });
    };

//finance
function fetch_requested_ssi()
{
   $.ajax({
    url:"../Finance/get_requested_ssi",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#requested_ssi').DataTable();
    }
   });  
}

function fetch_released_ssi()
{
   $.ajax({
    url:"../Ssi/get_released_ssi",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#released_ssi').DataTable();
    }
   });  
}




function fetch_check_ssi()
{
   $.ajax({
    url:"../Ssi/get_check_ssi",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#check_ssi').DataTable({
             "order": [] 
        });
    },
    error:function(data)
    {
      alert(data);
    }
   });  
}

function fetch_institutional_profile()
{
   $.ajax({
    url:"../Institutional_profile/get_institutional_profile",
    method:"POST",
    success:function(data)
    {
      $("#live_data").html(data);
      $('#institutional_profile_tbl').DataTable({
             "order": [] 
        });
    },
    error:function(data)
    {
      alert(data);
    }
   });  
}

  $(document).on('click','.ssi_approve',function()
    {
      var ssi = $(this).data("ssi");
      if(confirm("Are you sure you want to approve this ssi ?"))
      {
        $.ajax({
          url:"../Ssi/approve_ssi",
          method:"POST",
          data:{ssi: ssi},
          dataType:"text",
          success:function(data)
          {
            
            if(data=='success')
            {
              notify('SSI checking status successfully updated','success');
            }
            else
            {
              notify(data,'danger');
            }
            fetch_check_ssi();
            
          }
        });
      }
    });

  $(document).on('click','.ssi_req_approve',function()
    {
      var req = $(this).data("req");
      if(confirm("Are you sure you want to approve this ssi ?"))
      {
        $.ajax({
          url:"../Ssi/approve_req_ssi",
          method:"POST",
          data:{req: req},
          dataType:"text",
          success:function(data)
          {
             if(data=='success')
            {
            fetch_check_req_ssi();
            notify('SSI successfully recomended','success');
          }else{
              fetch_check_req_ssi();
              notify(data,'danger');
            }
         }
        });
      }
    });

    $(document).on('click','.ssi_final_check',function()
    {
      var req = $(this).data("req");
      if(confirm("Are you sure you want to approve this ssi ?"))
      {
        $.ajax({
          url:"../Ssi/approve_final_ssi",
          method:"POST",
          data:{req: req},
          dataType:"text",
          success:function(data)
          {
             if(data=='success')
            {
            fetch_final_ssi();
            notify('SSI successfully checked','success');
          }else{
            fetch_final_ssi();
              notify(data,'danger');
            }
         }
        });
      }
    });

    

    $(document).on('click','.ssi_final_approve',function()
    {
      var req = $(this).data("req");
      if(confirm("Are you sure you want to approve this ssi ?"))
      {
        $.ajax({
          url:"../Ssi/approve_final_ssi_director",
          method:"POST",
          data:{req: req},
          dataType:"text",
          success:function(data)
          {
             if(data=='success')
            {
            fetch_final_ssi();
            notify('SSI successfully approved','success');
          }else{
            fetch_final_ssi();
              notify(data,'danger');
            }
         }
        });
      }
    });

    $(document).on('click','.send_bod',function()
    {
      var req = $(this).data("req");
      if(confirm("Are you sure you want to send this ssi ?"))
      {
        $.ajax({
          url:"../Ssi/send_bod",
          method:"POST",
          data:{req: req},
          dataType:"text",
          success:function(data)
          {
             if(data=='success')
            {
            fetch_final_ssi();
            notify('SSI successfully send','success');
          }else{
            fetch_final_ssi();
              notify(data,'danger');
            }
         }
        });
      }
    });

    $(document).on('click','.ssi_submitted_approve',function()
    {
      var id = $(this).data("id");
      if(confirm("Are you sure you want to approve this ssi ?"))
      {
        $.ajax({
          url:"../Ssi/approve_submitted_ssi",
          method:"POST",
          data:{id: id},
          dataType:"text",
          success:function(data)
          {
             if(data=='success')
            {
            fetch_submitted_ssi();
            notify('SSI successfully approve','success');
          }else{
              fetch_submitted_ssi();
              notify(data,'danger');
            }
         }
        });
      }
    });

    $('#send_comm_form').on('submit',function(e){
      
        e.preventDefault();
        var post_url = '../Communications/send_comm';
        $.ajax({
            type : 'POST',
            url : post_url,
            data: new FormData($('#send_comm_form')[0]),
            cache: false,
            contentType: false,
            processData: false,
  
            success : function(data)
            {
              console.log(data);
              if(data=='success')
                        {
                        fetch_communications();
                        notify('Letter successfully send','success');
                        $('#send_comm_form')[0].reset();
                        $('#modal_comm').modal('toggle');
                      }else{
                        fetch_communications();
                          $('#send_comm_form')[0].reset();
                          $('#modal_comm').modal('toggle');
                          notify(data,'danger');
                        }   
            },
            error : function() {
                 notify('Error in submit ','danger');
            }
        });
          });

          $('#send_lett_form').on('submit',function(e){
            
              e.preventDefault();
              var post_url = '../Send_letter/send_lett';
              $.ajax({
                  type : 'POST',
                  url : post_url,
                  data: new FormData($('#send_lett_form')[0]),
                  cache: false,
                  contentType: false,
                  processData: false,
        
                  success : function(data)
                  {
                    console.log(data);
                    if(data=='success')
                              {
                              fetch_send_letter();
                              notify('Letter successfully send','success');
                              $('#send_lett_form')[0].reset();
                              $('#modal_lett').modal('toggle');
                            }else{
                              fetch_send_letter();
                                $('#send_lett_form')[0].reset();
                                $('#modal_lett').modal('toggle');
                                notify(data,'danger');
                              }   
                  },
                  error : function() {
                       notify('Error in submit ','danger');
                  }
              });
                });

    

    $('#add_approve_file_staff').on('submit',function(e){
      
        e.preventDefault();
        var post_url = '../Ssi/approve_submitted_ssi';
        $.ajax({
            type : 'POST',
            url : post_url,
            data: new FormData($('#add_approve_file_staff')[0]),
            cache: false,
            contentType: false,
            processData: false,
  
            success : function(data)
            {
              if(data=='success')
                        {
                        fetch_submitted_ssi();
                        notify('SSI successfully approve','success');
                        $('#add_approve_file_staff')[0].reset();
                        $('#modal_approved_staff').modal('toggle');
                      }else{
                          fetch_submitted_ssi();
                          $('#add_approve_file_staff')[0].reset();
                          $('#modal_approved_staff').modal('toggle');
                          notify(data,'danger');
                        }   
            },
            error : function() {
                 notify('Error in submit ','danger');
            }
        });
          });

          $('#add_approve_file_coa').on('submit',function(e){
            
              e.preventDefault();
              var post_url = '../Ssi/approve_submitted_ssi';
              $.ajax({
                  type : 'POST',
                  url : post_url,
                  data: new FormData($('#add_approve_file_coa')[0]),
                  cache: false,
                  contentType: false,
                  processData: false,
        
                  success : function(data)
                  {
                    if(data=='success')
                              {
                              fetch_submitted_ssi();
                              notify('SSI successfully approve','success');
                              $('#add_approve_file_coa')[0].reset();
                              $('#modal_approved_coa').modal('toggle');
                            }else{
                                fetch_submitted_ssi();
                                $('#add_approve_file_coa')[0].reset();
                                $('#modal_approved_coa').modal('toggle');
                                notify(data,'danger');
                              }   
                  },
                  error : function() {
                       notify('Error in submit ','danger');
                  }
              });
                });

    $(document).on('click','.send_letter',function()
    {
      var id = $(this).data("id");
      var school = $(this).data("school");
      if(confirm("Are you sure you want to send letter?"))
      {
        $.ajax({
          url:"../Ssi/send_letter",
          method:"POST",
          data:{id: id,school: school},
          dataType:"text",
          success:function(data)
          {
            console.log(data)
             if(data=='success')
            {
            fetch_submitted_ssi();
            notify('Letter successfully send','success');
          }else{
              fetch_submitted_ssi();
              notify(data,'danger');
            }
         }
        });
      }
    });


  $(document).on('click','#ins_profile_btn',function()
    {
        $("#ins_profile_modal").modal({
          "backdrop": "static" 
        });/*
         var title = $(this).data('title');
         var ssi_code = $(this).data('ssi');
         var program = $(this).data('program');
         $("#title").val(title);
         $("#ssi_code").val(ssi_code);
         $("#program").val(program);*/
    });

  $('#req_check_ip').on('submit',function(e){
      e.preventDefault();
       var post_url = $(this).attr("action");;
      $.ajax({
          type : 'POST',
          url : post_url,
          data: new FormData($('#req_check_ip')[0]),
          cache: false,
          contentType: false,
          processData: false,
          beforeSend:function(){
           $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#req_check_ip_btn').hide();
          },
          success : function(data){
            if(data=="success")
            {
              $('#feedback').html('');
              $('#req_check_ip_btn').html('Please wait for the confirmation');
              $('#req_check_ip_btn').show(500);
              $('#req_check_ip_btn').attr("disabled", true);
              $('#institutional_profile').attr("disabled", true);
              notify('Successfully submitted. Please wait for the confirmation','success');
            }
            else if(data=='failed2')
            {
              $('#feedback').html('');
              notify('You have already requested for checking. Please wait for the confirmation.','danger');
              $('#req_check_ip_btn').show(500);
            }
            else
            {
              $('#feedback').html('');
              notify('Please check your file and please try again.','danger');
              $('#req_check_ip_btn').show(500);
            }
                             
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });


   $('#update_payment_form').on('submit',function(e){
      e.preventDefault();
       var post_url = $(this).attr("action");;
      $.ajax({
          type : 'POST',
          url : post_url,
          data: new FormData($('#update_payment_form')[0]),
          cache: false,
          contentType: false,
          processData: false,
          beforeSend:function(){
          //  $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('#btn-payment').hide();
          },
          success : function(data){
            if(data=="success")
            {
              $('#feedback').html('');
              $('#btn-payment').html('Please wait for the confirmation');
              $('#btn-payment').show(500);
              $('#btn-payment').attr("disabled", true);
              $('#btn-payment').attr("disabled", true);
              notify('Successfully submitted. Please wait for the confirmation','success');
            }
            // else if(data=='failed2')
            // {
            //   $('#feedback').html('');
            //   notify('You have already requested for checking. Please wait for the confirmation.','danger');
            //   $('#req_check_ip_btn').show(500);
            // }
            else
            {
              $('#feedback').html('');
              notify('Please check your file and please try again.','danger');
              $('#btn-payment').show(500);
            }
                             
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
  });


  $(document).on('click','.ip_approve',function()
    {
      var ssi = $(this).data("ssi");
      var email = $(this).data("email");
      var accesskey = $(this).data("accesskey");
      if(confirm("Are you sure you want to approve this ssi ?"))
      {
        $.ajax({
          url:"../Institutional_profile/approve_ip",
          method:"POST",
          data:{ssi: ssi,email:email,accesskey:accesskey},
          dataType:"text",
          success:function(data)
          {
            
            if(data=='success')
            {
              notify('Institutional profile successfully approved','success');
            }
            else
            {
              notify(data,'danger');
            }
            fetch_institutional_profile()
          }
        });
      }
    });

  $(document).on('click','.docs_approve',function()
    {
      var ssi = $(this).data("ssi");
      var email = $(this).data("email");
      var accesskey = $(this).data("accesskey");
      if(confirm("Are you sure you want to approve this documents ?"))
      {
        $.ajax({
          url:"../Submitted_documents/approve_documents",
          method:"POST",
          data:{ssi: ssi,email:email,accesskey:accesskey},
          dataType:"text",
          success:function(data)
          {
            
            if(data=='success')
            {
              notify('Documents successfully approved','success');
            }
            else
            {
              notify(data,'danger');
            }
            fetch_submitted_documents()
          }
        });
      }
    });

  $('#assign_accreditor_form').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Assign_accreditor/assign';
      if(confirm("Are you sure you the accreditor cannot be updated?"))
      {
        $.ajax({
            type : 'POST',
            url : post_url,
            data: $('#assign_accreditor_form').serialize(),
            beforeSend:function(){
              $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
              $('.confirm_accreditor').hide();
            },
            success : function(data){
                if(data=='success')
                {
                  $('#feedback').html('<center>You have already assigned accreditor for this ssi</center>');
                  notify('Successfully assigned','success');
                }
                else
                {
                  $('#feedback').html('');
                  $('.confirm_accreditor').show(500);
                  notify(data,'danger');
                }
                
            },
            error : function() {
                 notify('Error in submit ','danger');
            }
        });
      }
  });

  $('#assign_accreditor_form2').on('submit',function(e){
    e.preventDefault();
    var post_url = '../Assign_accreditor/assign2';
    if(confirm("Are you sure you to assign these accreditors?"))
    {
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#assign_accreditor_form2').serialize(),
          beforeSend:function(){
            $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            $('.confirm_accreditor').hide();
          },
          success : function(data){
              if(data=='success')
              {
                $('#feedback').html('<center>You have already assigned accreditor for this ssi</center>');
                notify('Successfully assigned','success');
              }
              else
              {
                $('#feedback').html('');
                $('.confirm_accreditor').show(500);
                notify(data,'danger');
              }
              
          },
          error : function() {
               notify('Error in submit ','danger');
          }
      });
    }
});



    $('#form_access_key3').on('submit',function(e){
      e.preventDefault();
      var post_url = '../Ssi_accreditor/access';
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#form_access_key3').serialize(),
          beforeSend:function(){
            // $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            // $('#access_key_btn').hide();
          },
          success : function(data){
              $('#form_access_key3')[0].reset();

              if(data == 'error'){
                notify('Wrong Access Key ','danger');
              }else{
                window.location.href = data;
              }
             
          },
          error : function(x) {
              console.log(x);
               notify('Error in submit ','danger');
          }
      });
  });

    $(document).on('click','#btn_access_key_3',function()
    {

         var level = $(this).data('level');
         var program = $(this).data('program');
         var ssicode = $(this).data('ssicode');
         var email_school = $(this).data('email');
         $("#ssicode").val(ssicode);
         $("#level").val(level);
         $("#program").val(program);
         $("#email_school").val(email_school);
    });

    function fetch_ssi_accreditation()
    {
       $.ajax({
        url:"../Ssi_accreditor/get_ssi_accreditation",
        method:"POST",
        success:function(data)
        {
          $("#live_data").html(data);
           $('#ssi_accreditation').DataTable();
        }
       });  
    }

    $('#submit_ssi_summary2').on('submit',function(e){
      e.preventDefault();
      var post_url = $(this).attr("action");;
       if(confirm("Are you sure you want to submit? Note: You cannot update your answers anymore."))
      {
      $.ajax({
          type : 'POST',
          url : post_url,
          data: $('#submit_ssi_summary2').serialize(),
          beforeSend:function(){
            // $('#feedback').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
            // $('#access_key_btn').hide();
          },
          success : function(data){
              if(data == 'success'){
                notify('SSI Questionnaire Successfully Submitted ','success');
                $('#submit_ssi_question').hide();
                $('#feedback3').html('<label>You have already submitted the SSI Questionnaire.</label>');
              }else{
                notify('You havent answered all the section.','danger');
              }
             
          },
          error : function(x) {
              console.log(x);
               notify('Error in submit ','danger');
          }
      });
    }
  });

  $('#submit_ssi_summary3').on('submit',function(e){
      e.preventDefault();
      var post_url = $(this).attr("action");;
       if(confirm("Are you sure you want to submit? Note: You cannot update your answers anymore."))
      {
      $.ajax({
          type : 'POST',
          url : post_url,
          data: new FormData($('#submit_ssi_summary3')[0]),
          cache: false,
          contentType: false,
          processData: false,
          beforeSend:function(){
            $('#feedback2').html('<center><img src="../../../ssi/assets/img/loading_dots.gif" height="50px"></center>');
             $('#submit_ssi').hide();
          },
          success : function(data){
              if(data == 'success'){
                notify('SSI Questionnaire Successfully Submitted ','success');
                $('#submit_ssi').hide();
                $('.documents').hide();
                $('#feedback2').html('<center><h4>You have already submitted this ssi</h4></center>');
              }else{
                $('#feedback2').html('');
                $('#submit_ssi').show(300);
                notify('You havent answered all the section.','danger');
              }
             
          },
          error : function(x) {
              console.log(x);
               notify('Error in submit ','danger');
          }
      });
    }
  });


