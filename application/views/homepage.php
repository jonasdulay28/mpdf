<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>JPG to PDF</title>
        <link rel="stylesheet" href="<?php echo base_url()?>bootstrap/css/bootstrap.min.css">
        <link href="<?php echo base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="<?php echo base_url()?>assets/js/html5shiv.js"></script>
        <script src="<?php echo base_url()?>assets/js/respond.min.js"></script>
        <![endif]-->
    </head><!--/head-->

    <body>
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="margin-top:30%;">
                    <div class="panel-body">
                        <h1>JPG to PDF</h1>
                        <?php echo form_open_multipart('Home/upload_file','method="POST"'); ?>
                            <input type="file" name="images[]" class="form_control" multiple><br>
                            <input type="submit" name="upload_file_btn" class="btn btn-primary" value="submit">
                        <?php echo form_close()?>
                    </div>
                </div>
            </div>
        </div>
        
        <form action="http://pdfformpro.devs/functions/signup">
            <div id="register" class="form-signin">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="glyphicon"><img src="http://pdfformpro.devs/assets/images/fullname-icon.png" alt="Fullname Icon" /></span> 
                    </div>
                    <input class="form-control" id="fullname" name="fullname" type="text" Placeholder="Enter Fullname"/>
                </div>

                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="glyphicon"><img src="http://pdfformpro.devs/assets/images/emailAddress-icon.png" alt="Email Address Icon" /></span> 
                    </div>
                    <input class="form-control" id="email" name="email" type="text" Placeholder="Enter Email*"/>
                </div>


                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="glyphicon"><img src="http://pdfformpro.devs/assets/images/password-icon.png" alt="password Icon" /></span> 
                    </div>
                    <input class="form-control" id="password" name="password" type="password" Placeholder="Enter Password*"/>
                </div>

                <input type="text" name="type" value="Annual">
                <input type="text" name="plan" value="12 Months">
                <div>
                    <label class="terms">
                        By continuing, I accept and agree to <a href="http://pdfformpro.devs/terms_of_service" target="_blank">"Terms of Use"</a>.
                    </label>
                </div>



                <button id="btnsignup" class="btn btn-signup-reg btn-lg ladda-button" data-style="expand-left" data-size="l" type="submit"><span class="ladda-label" style="text-align: center">SIGN UP NOW</span></button>

            </div>
        </form>
        <form action="http://pdfformpro.devs/functions/login" method="POST">
            <div id="signin" class="form-signin">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="glyphicon"><img src="http://pdfformpro.devs/assets/images/fullname-icon.png" alt=""></span> 
                    </div>
                    <input class="form-control" id="s_email" name="email" type="text" Placeholder="Enter Email*"/>
                </div>

                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="glyphicon"><img src="http://pdfformpro.devs/assets/images/password-icon.png" alt=""></span> 
                    </div>
                    <input class="form-control" id="s_password" name="password" type="password" Placeholder="Enter Password*"/>
                </div>
                <input type="text" name="type" value="12 Months">
                <input type="text" name="plan" value="Annual">

                <div class="checkbox">
                    <label>
                    <input type="checkbox" value="remember-me">
                    Remember me </label>
                </div>
                <span class="lbl-forgot"><a href="login/forgot_password">Forgot Password?</a></span>
                
                
                
                <button id="btnlogin" class="btn btn-signup-reg ladda-button" data-style="expand-left" data-size="l" type="submit"><span class="ladda-label" style="text-align: center">SIGN IN</span></button>
            </div>
        </form>
        <script src="<?php echo base_url()?>plugins/jQuery/jquery-3.1.1.min.js"></script>
    	<script src="<?php echo base_url()?>bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>