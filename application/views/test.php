<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>position demo</title>
  <style>
  div {
    padding: 15px;
  }
  p {
    margin-left: 10px;
  }
  </style>
  <link rel="stylesheet" href="<?php echo base_url()?>bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>jquery-ui/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url()?>jquery-ui/jquery-ui.theme.css">
  <script src="<?php echo base_url()?>plugins/jQuery/jquery-3.1.1.min.js"></script>
  <script src="<?php echo base_url()?>jquery-ui/jquery-ui.js"></script>
  <script src="<?php echo base_url()?>bootstrap/js/bootstrap.min.js"></script>
  <script>
  $( function() {
    $('#draggable').draggable({ containment: "parent" });
  } );
  </script>
  <style type="text/css">
    
    .draggable_container
    {
      border: 1px solid red !important;
      height: 500px;
      padding: 0px;
    }
    .drag_me
    {
      width:100px;
    }

    #dragThis {
    width: 6em;
    height: 6em;
    padding: 0.5em;
    border: 3px solid #ccc;
    border-radius: 0 1em 1em 1em;
    background-color: #fff;
    background-color: rgba(255,255,255,0.5);
}

#dropHere {
    width: 12em;
    height: 12em;
    padding: 0.5em;
    border: 3px solid #f90;
    border-radius: 1em;
    margin: 0 auto;
}
  </style>
</head>
<body>
  <div id="dragThis">
    <ul>
        <li id="posX"></li>
        <li id="posY"></li>
        <li id="finalX"></li>
        <li id="finalY"></li>
    </ul>
</div>
  <div class="container ">
    <div class="col-md-8 col-md-offset-2 draggable_container">
      <div id="draggable" class="ui-widget-content drag_me">
        <p>Drag me around</p>
      </div>
    </div>
  </div>
  <button id="button">asdasd</button>
<script>
  $('#dragThis').draggable(
    {
        containment: $('body'),
        drag: function(){
            var offset = $(this).offset();
            var xPos = offset.left;
            var yPos = offset.top;
            $('#posX').text('x: ' + xPos);
            $('#posY').text('y: ' + yPos);
        },
        stop: function(){
            var finalOffset = $(this).offset();
            var finalxPos = finalOffset.left;
            var finalyPos = finalOffset.top;

    $('#finalX').text('Final X: ' + finalxPos);
    $('#finalY').text('Final X: ' + finalyPos);
        }
    });
  $(".draggable_container").droppable({
        accept: '#dragThis',
        over : function(){
            $(this).animate({'border-width' : '5px',
                             'border-color' : '#0f0'
                            }, 500);
            $('#dragThis').draggable('option','containment',$(this));
        }
    });
  $( "#draggable" ).draggable({
      
      drag: function() {
      },
      stop: function() {
        var position = $("#draggable:first").position();

        console.log(position);
      }
    });


      $(document).ready(function() {
        $('#button').click(function(){
        var toAdd = $('input[name=annotateItem]').val();
        // $('.margin').append("<div class='note'>"+toAdd+"</div>");
                  
        // (1) create new element
        var $item = $('<div class="note"/>')
            .html('<input type="text" class="form-input" style="top:0;left:0;">');

        // (2) make it draggable
        $item.draggable();          

        // (3) append it to the document
        $item.appendTo('.draggable_container');
                  
        // (4) optional: empty input field
       // $('input[name=annotateItem]').val('');
        });
      });

</script>
 
</body>
</html>