<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>JPG to PDF</title>
        <link rel="stylesheet" href="<?php echo base_url()?>bootstrap/css/bootstrap.min.css">
        <link href="<?php echo base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="<?php echo base_url()?>assets/js/html5shiv.js"></script>
        <script src="<?php echo base_url()?>assets/js/respond.min.js"></script>
        <![endif]-->
    </head><!--/head-->

    <body>
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="margin-top:30%;">
                    <div class="panel-body">
                        <?php 
                            $images = explode(";",$content);
                            foreach($images as $key)
                            {
                        ?>
                            <img src="<?php echo base_url('uploads/').$key?>" class="img-responsive center-block">
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        

        <script src="<?php echo base_url()?>plugins/jQuery/jquery-3.1.1.min.js"></script>
    	<script src="<?php echo base_url()?>bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>