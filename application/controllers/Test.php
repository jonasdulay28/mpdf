<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
	}

	public function index()
	{
		//echo base_url();
	 $this->load->view('test');
	}


}