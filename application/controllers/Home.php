<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
	}

	public function index()
	{
		//echo base_url();
	 $this->load->view('homepage');
	}

	public function upload_file()
	{
		require_once APPPATH . '/vendor/autoload.php';
		$status = $this->do_upload($_FILES['images']);
		$decode_status = json_decode($status, true);
		if($decode_status["message"] == "success")
		{
			// Create an instance of the class:
		  $mpdf = new \Mpdf\Mpdf();
			/*
			RESIZE YUNG IMAGE
			$mpdf->AddPage();
			$mpdf->Image($image,0,0,170,170);*/
		  $filename = time()."_pdffile.pdf";
	 	  $data["content"]= $decode_status["images"];
		  $html = $this->load->view('jpgtopdf',$data,true);

		  // Write some HTML code:
		  $mpdf->WriteHTML($html);
		  //download it D save F.
		  $mpdf->Output("./uploads/".$filename, "F");
			echo $decode_status["message"];
		}
		else
		{
			echo $decode_status["message"];
		}
		
	}

	private function do_upload($files)
	{
		$config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size'] = 0;
    $config['max_width'] = 0;
    $config['max_height'] = 0;
    $config['remove_spaces'] = TRUE;
    $this->load->library('upload', $config);

    $images = array(); //stores image file name
    $status = true;

    //uploading files
    foreach ($files['name'] as $key=>$image)
    {
    	$_FILES['images[]']['name']= $files['name'][$key];
      $_FILES['images[]']['type']= $files['type'][$key];
      $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
      $_FILES['images[]']['error']= $files['error'][$key];
      $_FILES['images[]']['size']= $files['size'][$key];

      $image = preg_replace('/\s+/','',$image);
      $fileName = time() .'_'. $image;

      

      $config['file_name'] = $fileName;
      $this->upload->initialize($config);
      if ($this->upload->do_upload('images[]')) 
          $images[] = $fileName;
      else 
          $status = false;
	  }

	  // check if all images are validated correctly
	  if($status)
	  {
	  	$data["images"] = implode(";", $images);
	  	$data["message"] = "success";
	  	return json_encode($data);
	  }
	  else
	  {
	  	//delete yung mga files kase failed sa validation 
	  	foreach($images as $key)
	  	{
	  		unlink('uploads/'.$key);
	  	}
	  	$data["message"] = "Invalid upload";
	  	return json_encode($data);
	  }
	}


/* function my_mPDF(){
 		 require_once APPPATH . '/vendor/autoload.php';
		// Create an instance of the class:
		 $mpdf = new \Mpdf\Mpdf();

		
		$filename = time()."_order.pdf";
	 	$data["content"]= "<h1>aasdasdas</h1>";
		$html = $this->load->view('unpaid_voucher',$data,true);

		// Write some HTML code:
		$mpdf->WriteHTML($html);

		
		//download it D save F.
		$mpdf->Output("./uploads/".$filename, "F");
	}
*/



}